<?
/**
 * Google Cloud Service class
 * Assumption: GCP Nearline Storage - https://cloud.google.com/storage/pricing
 **/
class GCP {

  //GCP storage is in decimal
  //10 MB = 0.01 GB (in decimal)
  //10 MB = 0.009765625 GB (in binary)
  protected $_storagePerStudy = 0.01;

  //1GB of storage cost 0.10 USD per month
  protected $_storageCostPerGbInUsd = 0.1;

  /**
   * Compute storage costing for the month provided the number of study per month
   * @param int $studyNum number of study per month
   * @param int storage costing for the month in USD
   **/
  public function computeCost($studyNum) {
    return ceil($studyNum * $this->_storagePerStudy) * $this->_storageCostPerGbInUsd;
  }
    
}