<?php
/**
 * Plugin Name: Cost Forecast WordPress Plugin
 * Description: Plugin to calculate future costing for Lifetrack Medical Systems
 * Version: 1.0
 * Author: Jacquelyn Tandang
 * Author URI: https://www.linkedin.com/in/jatandang/
 */


//CREATES THE API ENDPOINT TO COMPUTE THE COSTING
add_action( 'rest_api_init', 'register_route' );

//Registers new API endpoints
function register_route() {

  //Sample request: /wp-json/calculate/index
  //@TODO: How to register route/endpoint without namespace/route?
  register_rest_route( 'calculate', 'index', array(
      'methods' => 'GET',
      'callback' => 'calculate_cost',
    )
  );
}

require 'CostCalculator.php';
require 'GCP.php';
require 'AWS.php';

function calculate_cost($params) {

  $costCalculator = new CostCalculator();

  $cost = $costCalculator->calculate(
    $params['dailyNumStudy'], 
    $params['monthlyStudyGrowth'], 
    $params['monthsForecastNum']
  );

  $return = array(
    'data' => array(
      'monthly' => $cost->breakdown, 
      'cost' => $cost->total
    )
  );
  
  return rest_ensure_response($return);

}


//ADDS THE JS & CSS 

add_action('wp_enqueue_scripts', 'set_up_scripts');

function set_up_scripts() {

  //Include the css file
  wp_register_style( 'styles', plugin_dir_url( __FILE__ ) . 'css/styles.css'  );
  wp_enqueue_style( 'styles' );

  //Include parsley script
  wp_enqueue_script( 
    'parsley-script', 
    plugin_dir_url( __FILE__ ) . 'js/parsley.js', array( 'jquery' ), 
    '1.0.0', 
    true 
  );

  //Include ajax script
  wp_enqueue_script( 
    'ajax-script', 
    plugin_dir_url( __FILE__ ) . 'js/ajax-script.js', array( 'jquery' ), 
    '1.0.0', 
    true 
  );
    
}


//AUTOMATICALLY CREATES A PAGE /cost-calculator ON PLUGIN ACTIVATION
register_activation_hook(__FILE__, 'create_page');

function create_page() {

  //@TODO: How to get this from a file instead
  $html = 
  '
    <div id="calculator">

      <form id="cost-calculator-form" data-parsley-validate="">
        <div class="form-group">
          <label >Daily number of studies</label>
          <input type="text" class="form-control" id="num-study-daily" required="" min="0"  
            data-parsley-validation-threshold="1" 
            data-parsley-trigger="keyup" 
            data-parsley-type="number"
          >
        </div>
        <div class="form-group">
          <label >Monthly study growth (%)</label>
          <input type="text" class="form-control" id="study-growth-monthly" required="" min="0"  
            data-parsley-validation-threshold="1" 
            data-parsley-trigger="keyup" 
            data-parsley-type="number"
          >
        </div>
        <div class="form-group">
          <label >Number of months to forecast</label>
          <input type="text" class="form-control" id="num-months-forecast" required="" min="0" 
            data-parsley-validation-threshold="1" 
            data-parsley-trigger="keyup" 
            data-parsley-type="number"
          >
        </div>
        <button type="submit" class="btn btn-primary" >Calculate</button>
      </form>

    </div>

    <div id="calculator-result" >
      <div id="loading-gif" >
        <img src="' . plugin_dir_url( __FILE__ ) . '/img/loader.gif" />
      </div>
      <div id="computation-table"></div>
    </div>
  ';

  $post_details = array(
    'post_title'    => 'Cost Calculator',
    'post_content'  => $html,
    'post_status'   => 'publish',
    'post_author'   => 1,
    'post_type' => 'page'
  );

  wp_insert_post( $post_details );
}
