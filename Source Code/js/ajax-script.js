

(function($) {

  //@TODO: How to separate logic from html in JS in WordPress

  var Calculator = {
    init: function (settings) {

      this.currency = settings.currency;
      this.formSelector = settings.formSelector;
      this.numstudySelector = settings.numstudySelector;
      this.monthlyStudyGrowthSelector = settings.monthlyStudyGrowthSelector;
      this.monthsForecastNumSelector = settings.monthsForecastNumSelector;
      this.resultDivSelector = settings.resultDivSelector;
      this.loadingImageSelector = settings.loadingImageSelector;

      Calculator.bindInputMasking(settings.formSelector);
      Calculator.bindSubmitEvent(settings.formSelector);
      
    },
    bindInputMasking: function (formSelector) {

      //Input masking without use of 3rd party plugins
      $("form" + formSelector + " input").keyup(function() {
        this.value = this.value.replace(/[^0-9]/g, '');
      });

    },
    bindSubmitEvent: function (formSelector) {

      $("form" + formSelector).parsley().on('form:submit', function() {

        Calculator.showLoading();
        Calculator.computeAJAX();

        // Don't submit form
        return false;
      });

    },
    showLoading: function() {
      $(Calculator.loadingImageSelector).show();
      $(Calculator.resultDivSelector).html('');
    },
    computeAJAX: function () {
      $.ajax({
          url: "/wp-json/calculate/index", 
          data: { 
            dailyNumStudy: $(Calculator.numstudySelector).val(), 
            monthlyStudyGrowth : $(Calculator.monthlyStudyGrowthSelector).val(),
            monthsForecastNum: $(Calculator.monthsForecastNumSelector).val()
          },
          success: function(result){
            $(Calculator.loadingImageSelector).hide();
            $(Calculator.resultDivSelector).html(Calculator.generateTableHTML(result.data));
          }
      });
    },
    generateTableHTML: function (data) {

      var html = "<table>";

      html += "<tr>";
      html += "<td>Month</td>";
      html += "<td>Number of days</td>";
      html += "<td>Number of studies</td>";
      html += "<td>RAM cost</td>";
      html += "<td>Storage cost</td>";
      html += "</tr>";

      for (const month of data.monthly) {
        html += Calculator.generateTableRow(month);
      }

      html += "<tr>";
      html += "<td colspan='5'></td>";
      html += "</tr>";

      html += "<tr>";
      html += "<td>Total Cost</td>";
      html += "<td></td>";
      html += "<td></td>";
      html += "<td></td>";
      html += "<td>" + data.cost + " " + Calculator.currency+ "</td>";
      html += "</tr>";

      html += "</table>";

      return html;
    },
    generateTableRow: function (month) {

      html = "<tr>";
      html += "<td>" + month.month + "</td>";
      html += "<td>" + month.num_days + "</td>";
      html += "<td>" + month.study + "</td>";
      html += "<td>" + month.ram_cost + "</td>";
      html += "<td>" + month.storage_cost + "</td>";
      return html += "</tr>";
    }

  }

  Calculator.init({
    currency: "USD",
    formSelector: "#cost-calculator-form",
    numstudySelector: "#num-study-daily",
    monthlyStudyGrowthSelector: "#study-growth-monthly",
    monthsForecastNumSelector: "#num-months-forecast",
    resultDivSelector: "#computation-table",
    loadingImageSelector: "#loading-gif"
  });


})( jQuery );