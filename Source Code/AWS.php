<?
/**
 * Amazon Web Service class
 **/
class AWS {

  //The cost of 1GB of RAM per hour is 0.00553 USD
  protected $_ramCostPerGbPerHourInUsd = 0.00553;

  /**
   * Converts MB to GB
   * @param float $mb MB input
   * @return float GB equivalent
   **/
  protected function _convertMbToGB($mb) {
    return $mb * 0.001;
  }

  /**
   * Computes the cost per hour given the number of study per hour
   * @param int $studyPerHour number of study per hour
   * @return int cost per hour in USD
   **/
  public function computeHourlyRate($studyPerHour) {

    //1000 studies require 500 MB RAM
    $ramNeededPerHourInMB = 500 * ceil($studyPerHour/1000);

    //Price is in GB. Convert ram to GB
    //Ram is sold min 1GB per hour. Round up.
    $ramNeededPerHourInGB = ceil($this->_convertMbToGB($ramNeededPerHourInMB));

    return  $ramNeededPerHourInGB * $this->_ramCostPerGbPerHourInUsd; 
  }

  /**
   * Computes the cost per day given the number of study per hour
   * @param int $studyPerHour number of study per hour
   * @return int cost per day in USD
   **/
  public function computeDailyRate($studyPerHour) {
    return $this->computeHourlyRate($studyPerHour) * 24;
  }

  /**
   * Computes the cost given the number of days
   * @param int $studyPerHour number of study per hour
   * @param int $numDays number of days
   * @return int total cost in USD
   **/
  public function computeCost($studyPerHour, $numDays) { 
    return $this->computeDailyRate($studyPerHour) * $numDays;
  }

}