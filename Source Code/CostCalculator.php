<?

class CostCalculator {


  protected $_month;

  protected $_prevMonthStudyNum;

  protected $_monthlyBreakdown;

  protected $_totalRamCost;

  protected $_totalStorageCost;

  protected $_decimalPoint;


  function __construct() {

    $this->_totalRamCost = 0;
    $this->_totalStorageCost = 0;

    //Round up float up to 2 decimal places
    $this->_decimalPoint = 2;

    //Get first day of current month
    $this->_month = new DateTime();
    $this->_month->modify( 'first day of this month' );

    $this->_monthlyBreakdown = array();
    
  }

  /**
   * Moves the current month to the next month
   **/
  protected function _switchNextMonth() {
    $this->_month->modify( 'next month' );
  }

  /**
   * Returns the number of days in the current month
   * @return int number of days in the current month
   **/
  protected function _getMonthNumberOfDays() {
    return (int) $this->_month->format('t');
  }

  /**
   * Returns the current month and year as specified in the specifications document
   * @return string current month and year
   **/
  protected function _getMonth() {
    return $this->_month->format('F Y');
  }

  /**
   * Updates the value of the number of studies in the previous month
   * @param int $value number of studies
   **/
  protected function _updatePrevMonthStudyNum($value) {
    $this->_prevMonthStudyNum = $value;
  }

  /**
   * Computes the number of studies this month
   * @return int number of studies rounded up
   **/
  protected function _computeNumberOfStudyThisMonth() {
    $studyNum = $this->_prevMonthStudyNum + ($this->_prevMonthStudyNum * $this->_monthlyStudyGrowthPercentage);
    return ceil($studyNum);
  }

  /**
   * Computes the number of studies per hour
   * @param int $numberOfStudyPerMonth number of study per month
   * @param int $numberOfDays number of days in the month
   * @return int number of studies rounded up
   **/
  protected function _computeNumberOfStudyPerHour($numberOfStudyPerMonth, $numberOfDays) {
    return ceil ($numberOfStudyPerMonth / $numberOfDays / 24);
  }

  /**
   * Formats the integer value as specified in the specifications document
   * @param int $amt input unformatted amount
   * @return int formatted amount
   **/
  protected function _formatNumber($amt){
    return number_format($amt);
  }

  /**
   * Formats the money value as specified in the specifications document
   * @param float $amt input unformatted amount
   * @return string formatted amount
   **/
  protected function _formatMoney($amt) {
    return "$" . number_format($amt, $this->_decimalPoint, '.', ',');
  }

  /**
   * Returns the breakdown of the last computation per month
   * @return mixed array 
   **/
  protected function _getMonthlyBreakdown() {
    return $this->_monthlyBreakdown;
  }


  /**
   * Computes the forecasted cost provided the following input
   * @param int $dailyNumStudy daily number of study received
   * @param int $monthlyStudyGrowth percent monthly expected growth (ex. 100 for 100%)
   * @param int $monthsForecastNum number of months to forecast
   * @return string total cost
   **/
  public function calculate($dailyNumStudy, $monthlyStudyGrowth, $monthsForecastNum) {

    //Compute percent to float
    $this->_monthlyStudyGrowthPercentage = $monthlyStudyGrowth * 0.01;

    //Set current month study number
    $this->_updatePrevMonthStudyNum($this->_getMonthNumberOfDays() * $dailyNumStudy);

    for ($i=0; $i<$monthsForecastNum; $i++) {

      
      $numDays = $this->_getMonthNumberOfDays();
      $studyNumThisMonth = $this->_computeNumberOfStudyThisMonth();

      //Compute RAM costing (AWS)
      $aws = new AWS();
      $ramCost = $aws->computeCost(
        $this->_computeNumberOfStudyPerHour($studyNumThisMonth, $numDays), 
        $numDays
      );

      //Compute Storage costing (GCP)
      $gcp = new GCP();
      $storageCost = $gcp->computeCost($studyNumThisMonth);

      $this->_monthlyBreakdown[] = array(
        'month' => $this->_getMonth(),
        'num_days' => $numDays,
        'study' => $this->_formatNumber($studyNumThisMonth),
        'ram_cost' => $this->_formatMoney($ramCost, $this->_decimalPoint),
        'storage_cost' => $this->_formatMoney($storageCost, $this->_decimalPoint)
      ); 

      //Add this month's cost to total cost
      $this->_totalRamCost += $ramCost;
      $this->_totalStorageCost += $storageCost;

      $this->_updatePrevMonthStudyNum($studyNumThisMonth);
      $this->_switchNextMonth();
    }

    $cost = new stdClass();
    $cost->total = $this->_formatMoney($this->_totalRamCost + $this->_totalStorageCost);
    $cost->breakdown = $this->_getMonthlyBreakdown();

    return $cost;

  }
}